class AddDateToFixtureModel < ActiveRecord::Migration
  def change
    add_column :fixtures, :fixture_date, :date
  end

  def down
    remove_column :fixtures, :fixture_date
  end
end
