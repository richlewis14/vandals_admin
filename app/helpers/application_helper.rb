module ApplicationHelper

  def date_output(date)
    date.strftime('%e %B %Y') if date
  end

  def time_output(time)
    time.strftime('%H:%M') if time
  end

  def flash_class(level)
    case level
        when 'notice' then "alert alert-info"
        when 'success' then "alert alert-success"
        when 'error' then "alert alert-error"
        when 'alert' then "alert alert-error"
    end
  end
end
