class AdminController < ApplicationController
before_action :authenticate_user!
def index
  @fixtures = Fixture.order('fixtures.fixture_date DESC, fixtures.kickoff_time DESC')
  @tournament_fixture = @fixtures.group_by(&:tournament)
end
end
